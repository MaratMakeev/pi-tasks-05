#include <stdio.h>
#include <stdlib.h>
#include <locale.h> 
#define N 15
struct BOOK
{
	char title[256];
	char name[256];
	int year;
};
void RBook (FILE* fp,  struct BOOK*book)
{
	int i,j=0;
	char buf[254];	
	for(i=0;i<(N/3);i++)
	{
		fgets(book[i].title,256,fp);	
		fgets(book[i].name,256,fp);
		fgets(buf,256,fp);
		book[i].year=0;
		j=0;
		while(buf[j]!='\n')
		{
		book[i].year=book[i].year*10+buf[j]-'0';
		j++;
		}		
		printf("%s %s %d\n",book[i].title,book[i].name,book[i].year );
	}
}
void yearBook(struct BOOK*book)
{
	int i,min, max, modern=0, old=0;
	min=max=book[0].year;
	for(i=0;i<N/3;i++)
	{
		if (min>book[i].year)
		{
			min = book[i].year;
			old=i;
		}
		if (max<book[i].year)
		{
			max=book[i].year;
			modern=i;
		}
	}
	printf("\nСамая старая книга\n%s %s %d\nСамая новая книга \n%s %s %d\n",book[old].title,book[old].name,min,book[modern].title,book[modern].name,max);

}
void Sortbubble(struct BOOK*book)
{
	int i,j;
	struct BOOK temp;	
	for(i=0;i<(N/3)-1;i++)
		for(j=(N/3)-1;j>i;j--)
		{
			if( strcmp(book[j-1].name,book[j].name)>0)
			{
				temp=book[j];
				book[j]=book[j-1];
				book[j-1]=temp;
			}
		}
	printf("\nВ алфавитном порядке, по фамилиям авторов:\n" );
	for(i=0;i<(N/3);i++)
	{
		printf("%s %s %d\n",book[i].title,book[i].name,book[i].year );
	}
}
int main()
{
	struct BOOK book[N];
	FILE*fp;
	fp=fopen("input.txt","r");	
	if (fp==NULL)
	{
		perror("input.txt: ");
		return 1;
	}
	setlocale(LC_ALL, "Rus");
	RBook(fp,&*book);
	yearBook(&*book);
	Sort(&*book);	
	return 0;
}